            <ul class="subnav-hero-subnav" role="navigation">
                <li>
                    <a href="#" data-toggle="menu" role="button">MENU <i class="fi-list"></i></a>
                    <div class="dropdown-pane" data-position="bottom" data-alignment="center" data-options="closeOnClick: true;" id="menu" data-dropdown data-auto-focus="true">
                        <ul data-magellan>
                            <li role="menuitem">
                                <a href="/#what" onclick="$('#menu').foundation('close');">
                                    What Are These Crazy Oils Everyone Is Talking About?
                                </a>
                            </li>
                            <br>
                            <li role="menuitem">
                                <a href="/#why" onclick="$('#menu').foundation('close');">
                                    Why Do I Need Them?
                                </a>
                            </li>
                            <br>
                            <li role="menuitem">
                                <a href="/#brand" onclick="$('#menu').foundation('close');">
                                    Why Should I Care What Brand I Buy?
                                </a>
                            </li>
                            <br>
                            <li role="menuitem">
                                <a href="/#which" onclick="$('#menu').foundation('close');">Which Oils Should I Buy?</a>
                            </li>
                            <br>
                            <li role="menuitem">
                                <a href="/getting-started/">Where Should I Start?</a>
                            </li>
                            <br>
                            <li role="menuitem">
                                <a href="/#about" onclick="$('#menu').foundation('close');">About Kimberly</a>
                            </li>
                            <br>
                            <li role="menuitem">
                                <a href="/#contact" onclick="$('#menu').foundation('close');">Contact</a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>